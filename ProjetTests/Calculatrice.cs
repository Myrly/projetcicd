﻿namespace ProjetTests
{
    public class Calculatrice
    {
        public int Addition(int number1, int number2)
        {
            return number1 + number2;
        }
        
        public int Soustraction(int number1, int number2)
        {
            return number1 - number2;
        }
        
        public int Multiplication(int number1, int number2)
        {
            return number1 * number2;
        }
        
        public int Division(int number1, int number2)
        {
            
            if (number2 == 0)
            {
                throw new DivideByZeroException();
            }
            
            return number1 / number2;
        }
        
        public double DecimalDivision(int number1, int number2)
        {
            
            if (number2 == 0)
            {
                throw new DivideByZeroException();
            }
            
            return Math.Round((double) number1 / number2, 1);
        }
        
        public int Modulo(int number1, int number2)
        {
            
            if (number2 == 0)
            {
                throw new DivideByZeroException();
            }
            
            return number1 % number2;
        }
        
        public int Puissance(int number1, int number2)
        {
            return (int)Math.Pow(number1, number2);
        }
        
        public double RacineCarre(int number1)
        {
            return Math.Round(Math.Sqrt(number1), 2);
        }
        
        public int Factorielle(int number1)
        {
            int result = 1;
            for (int i = 1; i <= number1; i++)
            {
                result *= i;
            }
            return result;
        }
        
        public int FactorielleRecursive(int number1)
        {
            if (number1 == 0)
            {
                return 1;
            }
            else
            {
                return number1 * FactorielleRecursive(number1 - 1);
            }
        }
        
        public int Fibonacci(int number1)
        {
            if (number1 == 0)
            {
                return 0;
            }
            else if (number1 == 1)
            {
                return 1;
            }
            else
            {
                return Fibonacci(number1 - 1) + Fibonacci(number1 - 2);
            }
        }
        
        public int FibonacciIteratif(int number1)
        {
            int a = 0;
            int b = 1;
            int c = 0;
            for (int i = 0; i < number1; i++)
            {
                c = a + b;
                a = b;
                b = c;
            }
            return a;
        }

    }
}
