﻿
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ProjetTests.Tests;

[TestClass]
public class FizzBuzzTests
{
    [TestMethod]
    public void Calling_FizzBuzz_With1_Should_Return_1()
    {
        var fizzBuzz = new FizzBuzz();
        var result = fizzBuzz.Get(1);
        Assert.AreEqual(1, result);
    }
    
    [TestMethod]
    public void Calling_FizzBuzz_With2_Should_Return_2()
    {
        var fizzBuzz = new FizzBuzz();
        var result = fizzBuzz.Get(2);
        Assert.AreEqual(2, result);
    }
    
    [TestMethod]
    public void Calling_FizzBuzz_With3_Should_Return_Fizz()
    {
        var fizzBuzz = new FizzBuzz();
        var result = fizzBuzz.Get(3);
        Assert.AreEqual("Fizz", result);
    }
    
    [TestMethod]
    public void Calling_FizzBuzz_With5_Should_Return_Buzz()
    {
        var fizzBuzz = new FizzBuzz();
        var result = fizzBuzz.Get(5);
        Assert.AreEqual("Buzz", result);
    }
    
    [TestMethod]
    public void Calling_FizzBuzz_With6_Should_Return_Fizz()
    {
        var fizzBuzz = new FizzBuzz();
        var result = fizzBuzz.Get(6);
        Assert.AreEqual("Fizz", result);
    }
    
    [TestMethod]
    public void Calling_FizzBuzz_With10_Should_Return_Buzz()
    {
        var fizzBuzz = new FizzBuzz();
        var result = fizzBuzz.Get(10);
        Assert.AreEqual("Buzz", result);
    }
    
    [TestMethod]
    public void Calling_FizzBuzz_With15_Should_Return_FizzBuzz()
    {
        var fizzBuzz = new FizzBuzz();
        var result = fizzBuzz.Get(15);
        Assert.AreEqual("FizzBuzz", result);
    }
}