
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ProjetTests.Tests
{
    [TestClass]
    public class CalculatriceTests
    {
        [TestMethod]
        public void Calling_Addition_With_3_And_5_Should_Return_8()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Addition(3, 5);
            Assert.AreEqual(8, result);
        }

        [TestMethod]
        public void Calling_Addition_With_3_And_5_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Addition(3, 5);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Soustraction_With_5_and_3_Should_Return_2()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Soustraction(5, 3);
            Assert.AreEqual(2, result);
        }
        
        [TestMethod]
        public void Calling_Soustraction_With_5_and_3_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Soustraction(5, 3);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Soustaction_With_3_and_5_Should_Return_Negative_2()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Soustraction(3, 5);
            Assert.AreEqual(-2, result);
        }
        
        [TestMethod]
        public void Calling_Soustraction_With_3_and_5_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Soustraction(3, 5);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Multiplication_With_5_and_3_Should_Return_15()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Multiplication(5, 3);
            Assert.AreEqual(15, result);
        }
        
        [TestMethod]
        public void Calling_Multiplication_With_5_and_3_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Multiplication(5, 3);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Division_With_15_and_3_Should_Return_5()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Division(15, 3);
            Assert.AreEqual(5, result);
        }
        
        [TestMethod]
        public void Calling_Division_With_15_and_3_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Division(15, 3);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Division_With_15_and_0_Should_Return_Exception()
        {
            var calculatrice = new Calculatrice();
            Assert.ThrowsException<DivideByZeroException>(() => calculatrice.Division(15, 0));

        }
        
        [TestMethod]
        public void Calling_Division_With_0_and_3_Should_Return_0()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Division(0, 3);
            Assert.AreEqual(0, result);
        }
        
        [TestMethod]
        public void Calling_Division_With_0_and_3_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Division(0, 3);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Division_With_3_and_5_Should_Return_0()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Division(3, 5);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void Calling_Division_With_3_and_5_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Division(3, 5);
            Assert.IsInstanceOfType(result, typeof(int));
        }

        [TestMethod]
        public void Calling_Decimal_Division_With_15_and_3_Should_Return_5()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.DecimalDivision(15, 3);
            Assert.AreEqual(5, result);
        }
        
        [TestMethod]
        public void Calling_Decimal_Division_With_15_and_3_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.DecimalDivision(15, 3);
            Assert.IsInstanceOfType(result, typeof(double));
        }
        
        [TestMethod]
        public void Calling_Decimal_Division_With_15_and_0_Should_Return_Exception()
        {
            var calculatrice = new Calculatrice();
            Assert.ThrowsException<DivideByZeroException>(() => calculatrice.DecimalDivision(15, 0));

        }
        
        [TestMethod]
        public void Calling_Decimal_Division_With_0_and_3_Should_Return_0()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.DecimalDivision(0, 3);
            Assert.AreEqual(0, result);
        }
        
        [TestMethod]
        public void Calling_Decimal_Division_With_0_and_3_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.DecimalDivision(0, 3);
            Assert.IsInstanceOfType(result, typeof(double));
        }
        
        [TestMethod]
        public void Calling_Decimal_Division_With_3_and_5_Should_Return_0_dot_6()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.DecimalDivision(3, 5);
            Assert.AreEqual(0.6, result);
        }
        
        [TestMethod]
        public void Calling_Decimal_Division_With_3_and_5_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.DecimalDivision(3, 5);
            Assert.IsInstanceOfType(result, typeof(double));
        }
        
        [TestMethod]
        public void Calling_Modulo_With_15_and_3_Should_Return_0()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Modulo(15, 3);
            Assert.AreEqual(0, result);
        }
        
        [TestMethod]
        public void Calling_Modulo_With_15_and_3_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Modulo(15, 3);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Modulo_With_15_and_0_Should_Return_0()
        {
            var calculatrice = new Calculatrice();
            Assert.ThrowsException<DivideByZeroException>(() => calculatrice.Modulo(15, 0));
        }
        
        [TestMethod]
        public void Calling_Modulo_With_15_and_0_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            Assert.ThrowsException<DivideByZeroException>(() => calculatrice.Modulo(15, 0));

        }
        
        [TestMethod]
        public void Calling_Modulo_With_0_and_3_Should_Return_0()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Modulo(0, 3);
            Assert.AreEqual(0, result);
        }
        
        [TestMethod]
        public void Calling_Modulo_With_0_and_3_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Modulo(0, 3);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Modulo_With_3_and_5_Should_Return_3()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Modulo(3, 5);
            Assert.AreEqual(3, result);
        }
        
        [TestMethod]
        public void Calling_Modulo_With_3_and_5_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Modulo(3, 5);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Puissance_With_3_and_5_Should_Return_243()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Puissance(3, 5);
            Assert.AreEqual(243, result);
        }
        
        [TestMethod]
        public void Calling_Puissance_With_3_and_5_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Puissance(3, 5);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Puissance_With_3_and_0_Should_Return_1()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Puissance(3, 0);
            Assert.AreEqual(1, result);
        }
        
        [TestMethod]
        public void Calling_Puissance_With_3_and_0_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Puissance(3, 0);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Puissance_With_0_and_3_Should_Return_0()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Puissance(0, 3);
            Assert.AreEqual(0, result);
        }
        
        [TestMethod]
        public void Calling_Puissance_With_0_and_3_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Puissance(0, 3);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Puissance_With_0_and_0_Should_Return_1()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Puissance(0, 0);
            Assert.AreEqual(1, result);
        }
        
        [TestMethod]
        public void Calling_Puissance_With_0_and_0_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Puissance(0, 0);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        public void Calling_Racine_With_3_Should_Return_1_dot_73()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(3);
            Assert.AreEqual(1.73, result);
        }
        
        [TestMethod]
        public void Calling_Racine_With_3_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(3);
            Assert.IsInstanceOfType(result, typeof(double));
        }
        
        [TestMethod]
        public void Calling_Racine_With_0_Should_Return_0()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(0);
            Assert.AreEqual(0, result);
        }
        
        [TestMethod]
        public void Calling_Racine_With_0_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(0);
            Assert.IsInstanceOfType(result, typeof(double));
        }
        
        [TestMethod]
        public void Calling_Racine_With_1_Should_Return_1()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(1);
            Assert.AreEqual(1, result);
        }
        
        [TestMethod]
        public void Calling_Racine_With_1_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(1);
            Assert.IsInstanceOfType(result, typeof(double));
        }
        
        [TestMethod]
        public void Calling_Racine_With_2_Should_Return_1_dot_41()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(2);
            Assert.AreEqual(1.41, result);
        }
        
        [TestMethod]
        public void Calling_Racine_With_2_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(2);
            Assert.IsInstanceOfType(result, typeof(double));
        }
        
        [TestMethod]
        public void Calling_Racine_With_4_Should_Return_2()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(4);
            Assert.AreEqual(2, result);
        }
        
        [TestMethod]
        public void Calling_Racine_With_4_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(4);
            Assert.IsInstanceOfType(result, typeof(double));
        }
        
        [TestMethod]
        public void Calling_Racine_With_9_Should_Return_3()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(9);
            Assert.AreEqual(3, result);
        }
        
        [TestMethod]
        public void Calling_Racine_With_9_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(9);
            Assert.IsInstanceOfType(result, typeof(double));
        }
        
        [TestMethod]
        public void Calling_Racine_With_16_Should_Return_4()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(16);
            Assert.AreEqual(4, result);
        }
        
        [TestMethod]
        public void Calling_Racine_With_16_Should_Return_A_Number()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.RacineCarre(16);
            Assert.IsInstanceOfType(result, typeof(double));
        }
        
        [TestMethod]
        [DataRow(0, 1)]
        [DataRow(1, 1)]
        [DataRow(2, 2)]
        [DataRow(3, 6)]
        [DataRow(4, 24)]
        [DataRow(5, 120)]
        public void Calling_Factorielle_Should_Return_Expected(int number, int expected)
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Factorielle(number);
            Assert.AreEqual(expected, result);
        }
        
        [TestMethod]
        [DataRow(0, 1)]
        [DataRow(1, 1)]
        [DataRow(2, 2)]
        [DataRow(3, 6)]
        [DataRow(4, 24)]
        [DataRow(5, 120)]
        public void Calling_Factorielle_Should_Return_A_Number(int number, int expected)
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Factorielle(number);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
        [TestMethod]
        [DataRow(0, 1)]
        [DataRow(1, 1)]
        [DataRow(2, 2)]
        [DataRow(3, 6)]
        [DataRow(4, 24)]
        [DataRow(5, 120)]
        public void Calling_Factorielle_Recur_Should_Return_Expected(int number, int expected)
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.FactorielleRecursive(number);
            Assert.AreEqual(expected, result);
        }
        
        [TestMethod]
        [DataRow(0, 1)]
        [DataRow(1, 1)]
        public void Calling_Factorielle_Recur_Should_Return_A_Number(int number, int expected)
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.FactorielleRecursive(number);
            Assert.IsInstanceOfType(result, typeof(int));
        }
        
    }
}